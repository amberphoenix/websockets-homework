import { getTextForGameById } from "../helpers/dataHelper.mjs";

const username = sessionStorage.getItem("username");
const infoBox = document.getElementById("game-info");
const disconnectButton = document.getElementById("disconnect-button");
const readyButton = document.getElementById("ready-button");
const notReadyButton = document.getElementById("not-ready-button");
const usersList = document.getElementById("users-list");

if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

socket.on("USERNAME_ALREADY_EXIST", (username) => {
  window.alert(`${username} already exist!`);
  sessionStorage.clear();
  window.location.replace("/login");
});

socket.on("RENDER_USERS", (users) => {
  usersList.innerHTML = "";
  users.forEach((user) => {
    const userElement = document.createElement("div");
    userElement.setAttribute("id", "user");

    const userWrapper = document.createElement("div");
    userWrapper.className = "user-wrapper";
    const userStatus = document.createElement("div");
    if (user.isReady) {
      userStatus.style.backgroundColor = "#3bd965";
    } else {
      userStatus.style.backgroundColor = "#f54242";
    }
    userStatus.setAttribute("id", user.name);
    userStatus.className = "user-status";
    const userNameToDisplay = document.createElement("div");
    let userText = user.name;
    if (user.name === username) {
      userText += " (you)";
    }
    userNameToDisplay.innerText = userText;
    userWrapper.appendChild(userStatus);
    userWrapper.appendChild(userNameToDisplay);
    userElement.appendChild(userWrapper);

    const progressBar = document.createElement("progress");
    progressBar.setAttribute("value", "0");
    progressBar.setAttribute("max", "100");
    userElement.appendChild(progressBar);

    usersList.append(userElement);
  });
});

socket.on("START_TIMER", (time) => {
  disconnectButton.style.display = "none";
  notReadyButton.style.display = "none";

  const timer = document.createElement("div");
  timer.setAttribute("id", "start-game-timer");
  timer.innerText = time;
  infoBox.appendChild(timer);
});

socket.on("SET_TIMER", (time) => {
  const timer = document.getElementById("start-game-timer");
  timer.innerText = time;
});

socket.on("START_GAME", (textId) => {
  const timer = document.getElementById("start-game-timer");
  timer.style.display = "none";

  let textValue = "";

  getTextForGameById(textId)
    .then((res) => {
      textValue = res.text;
      const text = document.createElement("div");
      text.innerText = textValue;
      infoBox.appendChild(text);
    })
    .catch((error) => console.error(error));
});

const onClickDisconnectButton = () => {
  socket.emit("DISCONNECT_BUTTON", username);
  sessionStorage.clear();
  window.location.replace("/login");
};

const onClickReadyButton = () => {
  readyButton.style.display = "none";
  notReadyButton.style.display = "block";

  const status = document.getElementById(username);
  status.style.backgroundColor = "#3bd965";
  socket.emit("USER_READY", username);
};

const onClickNotReadyButton = () => {
  notReadyButton.style.display = "none";
  readyButton.style.display = "block";

  const status = document.getElementById(username);
  status.style.backgroundColor = "#f54242";
  socket.emit("USER_NOT_READY", username);
};

disconnectButton.addEventListener("click", onClickDisconnectButton);
readyButton.addEventListener("click", onClickReadyButton);
notReadyButton.addEventListener("click", onClickNotReadyButton);
