import gameHandler from "./gameHandler";

const usersArray = [];
let usersName = [];

export default (io) => {
  io.on("connection", (socket) => {
    const username = socket.handshake.query.username;

    usersName = usersArray.map((user) => user.name);

    if (usersName.includes(username)) {
      socket.emit("USERNAME_ALREADY_EXIST", username);
    } else {
      usersArray.push({ name: username, isReady: false });
      gameHandler(socket, usersArray);
    }

    socket.on("DISCONNECT_BUTTON", (userName) => {
      let indexToDelete = -1;
      usersArray.forEach((user, index) => {
        if (user.name === username) {
          indexToDelete = index;
        }
      });
      if (indexToDelete > -1) {
        usersArray.splice(indexToDelete, 1);
      }

      socket.broadcast.emit("RENDER_USERS", usersArray);
    });

    socket.on("disconnect", () => {
      let indexToDelete = -1;
      usersArray.forEach((user, index) => {
        if (user.name === username) {
          indexToDelete = index;
        }
      });
      if (indexToDelete > -1) {
        usersArray.splice(indexToDelete, 1);
      }
      socket.broadcast.emit("RENDER_USERS", usersArray);
    });
  });
};
