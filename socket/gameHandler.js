import { SECONDS_TIMER_BEFORE_START_GAME } from "./config";
import { texts } from "../data";

export default (socket, users) => {
  socket.emit("RENDER_USERS", users);
  socket.broadcast.emit("RENDER_USERS", users);

  socket.on("USER_READY", (username) => {
    let allUsersAreReady = true;
    users.forEach((user) => {
      if (user.name === username) {
        user.isReady = true;
      }
      if (user.isReady === false) {
        allUsersAreReady = false;
      }
    });
    socket.broadcast.emit("RENDER_USERS", users);
    if (allUsersAreReady) {
      startTimer(socket);
    }
  });

  socket.on("USER_NOT_READY", (username) => {
    users.forEach((user) => {
      if (user.name === username) {
        user.isReady = false;
      }
    });
    socket.broadcast.emit("RENDER_USERS", users);
  });
};

const startTimer = (socket) => {
  const textId = Math.floor(Math.random() * (texts.length - 1));
  console.log(texts.length);
  socket.broadcast.emit("START_TIMER", SECONDS_TIMER_BEFORE_START_GAME);
  socket.emit("START_TIMER", SECONDS_TIMER_BEFORE_START_GAME);
  let time = SECONDS_TIMER_BEFORE_START_GAME;
  const interval = setInterval(() => {
    socket.broadcast.emit("SET_TIMER", time);
    socket.emit("SET_TIMER", time);
    if (time <= 0) {
      clearInterval(interval);
      socket.broadcast.emit("START_GAME", textId);
      socket.emit("START_GAME", textId);
    }
    time--;
  }, 1000);
};
